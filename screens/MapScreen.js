import React, {useState, useEffect, useContext} from 'react';
import {PermissionsAndroid, StyleSheet} from 'react-native';
import { Button, Paragraph, Dialog, Portal, TextInput } from 'react-native-paper';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import { AuthContext } from '../navigation/Navigator';
import { getLocations, addLocation, deleteLocation } from '../services/mapsservice';
const mapStyle = [
   {
     "featureType": "all",
     "elementType": "labels.text.fill",
     "stylers": [
       {
         "saturation": 36
       },
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-43"
       }
     ]
   },
   {
     "featureType": "all",
     "elementType": "labels.text.stroke",
     "stylers": [
       {
         "visibility": "on"
       },
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-70"
       }
     ]
   },
   {
     "featureType": "all",
     "elementType": "labels.icon",
     "stylers": [
       {
         "visibility": "off"
       }
     ]
   },
   {
     "featureType": "administrative",
     "elementType": "geometry.fill",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-17"
       }
     ]
   },
   {
     "featureType": "administrative",
     "elementType": "geometry.stroke",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-66"
       },
       {
         "weight": 1.2
       }
     ]
   },
   {
     "featureType": "administrative",
     "elementType": "labels.text",
     "stylers": [
       {
         "lightness": "-63"
       }
     ]
   },
   {
     "featureType": "administrative",
     "elementType": "labels.text.fill",
     "stylers": [
       {
         "lightness": "43"
       },
       {
         "color": "#9dc0b9"
       }
     ]
   },
   {
     "featureType": "administrative",
     "elementType": "labels.text.stroke",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-69"
       }
     ]
   },
   {
     "featureType": "landscape",
     "elementType": "geometry",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-51"
       }
     ]
   },
   {
     "featureType": "poi",
     "elementType": "geometry",
     "stylers": [
       {
         "color": "#479687"
       },
       {
         "lightness": "-54"
       }
     ]
   },
   {
     "featureType": "poi",
     "elementType": "labels.text.fill",
     "stylers": [
       {
         "color": "#dae8e5"
       }
     ]
   },
   {
     "featureType": "road",
     "elementType": "labels.text.fill",
     "stylers": [
       {
         "color": "#9dc0b9"
       },
       {
         "lightness": "-23"
       },
       {
         "visibility": "on"
       }
     ]
   },
   {
     "featureType": "road",
     "elementType": "labels.text.stroke",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-66"
       }
     ]
   },
   {
     "featureType": "road.highway",
     "elementType": "geometry.fill",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-62"
       }
     ]
   },
   {
     "featureType": "road.highway",
     "elementType": "geometry.stroke",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-70"
       },
       {
         "weight": 0.2
       }
     ]
   },
   {
     "featureType": "road.arterial",
     "elementType": "geometry",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-60"
       }
     ]
   },
   {
     "featureType": "road.local",
     "elementType": "geometry",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-54"
       }
     ]
   },
   {
     "featureType": "transit",
     "elementType": "geometry",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-59"
       }
     ]
   },
   {
     "featureType": "water",
     "elementType": "geometry",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-61"
       }
     ]
   },
   {
     "featureType": "water",
     "elementType": "labels.text.fill",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-45"
       }
     ]
   },
   {
     "featureType": "water",
     "elementType": "labels.text.stroke",
     "stylers": [
       {
         "color": "#419d8c"
       },
       {
         "lightness": "-62"
       }
     ]
   }
 ]

const MapScreen = () => {
  const user = useContext(AuthContext);
  const [places, setPlaces] = useState([]);
  const [coords, setCoords] = useState();
  const [callout, setCallout] = useState(false);
  const [visible, setVisible] = useState(false);
  const [remData, setRemData] = useState(null);
  const [name, setName] = useState('');
  const [mushroomNames, setMushroomNames] = useState('');
  const [region, setRegion] = useState({
    latitude: 61.92410999999999,
    longitude: 25.748151,
    latitudeDelta: 7.0,
    longitudeDelta: 7.0,
  });

  useEffect(() => {
    requestPermission();
    getMarkers();
  },[])

  const requestPermission = async () => {
    try {
      const permissio = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
      if (permissio === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
            (position) => {
              // console.log(position);
              setRegion({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.5,
                longitudeDelta: 0.5,
              })
            },
            (error) => {
              // See error code charts below.
              console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      } else {
        console.log("Permission denied");
      }
    } catch (err) {
      console.log("Permission error: ", err)
    }
  }

  const getMarkers = async () => {
    const markers = await getLocations(user.uid);
    setPlaces(markers);
  }

  const addMarker = () => {
    const place = {
      id: Date.now(),
      coords: coords,
      name: name,
      mushrooms: mushroomNames,
    }
    addLocation(place, user.uid);
    getMarkers();
    hideDialog();
  }

  const removeMarker = async () => {
    await deleteLocation(remData.id, user.uid);
    getMarkers();
    hideRemDialog();
  }

  const onregionChange = (region) => {
    setRegion(region);
  }

  const showDialog = (coords) => {
    setVisible(true);
    // console.log(coords);
    setCoords(coords);
  }

  const hideDialog = () => {
    setVisible(false);
    setName('');
    setMushroomNames('');
  }

  const showRemDialog = (place) => {
    setRemData(place);
  }

  const hideRemDialog = () => {
    setRemData(null);
  }

  return (
    <MapView 
      provider={PROVIDER_GOOGLE}
      customMapStyle={mapStyle}
      style={{flex: 1}}
      showsUserLocation={true}
      region={region}
      onRegionChange={() => onregionChange()}
      onPress={(e) => {
        !callout ? showDialog(e.nativeEvent.coordinate) : setCallout(false)
      }}
    >
      {places.length > 0 ? places.map((place, index) => (
        <Marker 
          key={index}
          coordinate={place.coords}
          title={place.name}
          description={place.mushrooms}
          onPress={() => {
            setCallout(true)}
          }
          on
          onCalloutPress={() => showRemDialog(place)}
        />
      )) : null}
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Add Mushrooming Place</Dialog.Title>
          <Dialog.Content>
            <TextInput
              label="Name your mushrooming place"
              value={name}
              onChangeText={text => setName(text)}
            />
            <TextInput
              label="What mushrooms you can find?"
              value={mushroomNames}
              onChangeText={text => setMushroomNames(text)}
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>Cancel</Button>
            <Button onPress={addMarker}>Done</Button>
          </Dialog.Actions>
        </Dialog>
        <Dialog visible={remData !== null} onDismiss={hideRemDialog}>
          <Dialog.Title>Remove marker</Dialog.Title>
          <Dialog.Content>
            <Paragraph>{remData !== null ? remData.name : ""}</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideRemDialog}>Cancel</Button>
            <Button onPress={removeMarker}>Done</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </MapView>
  )
};

const styles = StyleSheet.create({});

export default MapScreen;
