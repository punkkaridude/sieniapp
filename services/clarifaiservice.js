import React, { useState, useEffect, useContext } from 'react';
import { Button, Text } from 'native-base';
import Clarifai from 'clarifai';
import ClarifaiContext from '../context/clarifai.context';
import { API_CLARIFAI } from '@env';
import { useNavigation } from '@react-navigation/native';

const ResultButton = () => {
    const navigation = useNavigation();
    return (
        <Button onPress={() => navigation.navigate("Mushrooms")}><Text>Result</Text></Button>
    );
}

const ClappAI = ({ base64 }) => {
    const {clarifaiResult, setClarifaiResult} = useContext(ClarifaiContext);
    const [res, setRes] = useState(false);

    useEffect(() => {
        setRes(false);
      },[])

    const clapp = new Clarifai.App({
        apiKey: API_CLARIFAI
    });

    clapp.inputs.search({ input: { base64: base64 } }).then(
        function (response) {
            const bestHit = response.hits[0].input.data.concepts[0].id;
            setClarifaiResult(bestHit);
            setRes(true);
        },
        function (err) {
            console.log("Clarifai error: ", err);
        }
    );

    return res && <ResultButton />
}

export default ClappAI;