import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConf from './icon-font.json';
const Icon = createIconSetFromFontello(fontelloConf, 'fewicons');

export default Icon;