import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Image, Modal, TouchableHighlight, View } from 'react-native';
import { Container, Text } from 'native-base';
import { AuthContext } from '../navigation/Navigator';
import Camera from '../components/Camera';
import ClarifaiContext from '../context/clarifai.context';
import Clapp from '../services/clarifaiservice';

const ImageModal = ({ image, base64, modalVisible, onClose }) => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Image source={{ uri: image }} style={styles.image} />
                    <TouchableHighlight style={styles.button} onPress={() => onClose()}>
                        <Text style={styles.textStyle}>X</Text>
                    </TouchableHighlight>
                    <Clapp base64={base64} />
                </View>
            </View>
        </Modal>
    );
}

const CameraScreen = () => {
    const [image, setImage] = useState(null);
    const [base64, setBase64] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const {clarifaiResult, setClarifaiResult} = useContext(ClarifaiContext);

    function onImg(data) {
        setImage(data.uri);
        setBase64(data.base64);
        setModalVisible(true);
    }

    function onClose() {
        setModalVisible(false);
    }

    return (
        <AuthContext.Consumer>
            {value => <Container>
                <ImageModal image={image} base64={base64} modalVisible={modalVisible} onClose={onClose} />
                <Camera onImg={onImg} />
            </Container>}
        </AuthContext.Consumer>
    );
};

const styles = StyleSheet.create({
    image: {
        resizeMode: 'cover',
        width: 280,
        height: 380,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalView: {
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 20,
        overflow: 'hidden',
        shadowColor: '#eee',
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 8,
    },
    button: {
        position: 'absolute',
        right: 0,
        backgroundColor: 'transparent',
        borderRadius: 20,
        padding: 6,
        paddingHorizontal: 10,
        elevation: 2,
        margin: 5
    },
    textStyle: {
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
    },
});

export default CameraScreen;
