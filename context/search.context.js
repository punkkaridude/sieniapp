import React, { createContext } from 'react';

const SearchContext = createContext({
    searchquery: "",
    setSearchquery: () => {},
});

export default SearchContext;