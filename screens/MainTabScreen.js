import * as React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import FrontpageScreen from './FrontpageScreen';
import AllMushroomsScreen from './AllMushroomsScreen';
import MapScreen from './MapScreen';
import MushroomScreen from './MushroomScreen';
import CameraScreen from './CameraScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Search from '../components/Search';

const Tab = createMaterialBottomTabNavigator();
const HomeStack = createStackNavigator();
const AllMushroomStack = createStackNavigator();
const MapStack = createStackNavigator();

const HomeStackScreen = ({navigation}) => (
    <HomeStack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: '#009387'
        },
        headerTintColor: '#fff',
    }}>
        <HomeStack.Screen 
            name="Home" 
            component={FrontpageScreen} options={{
            title: "SieniApp",
            headerLeft: () => (
                <Icon.Button name="menu" backgroundColor="#009387" size={25} onPress={() => {
                    navigation.openDrawer()
                }}/> 
            ),
            headerRight: (props) => (
                <Search {...props} navigation={navigation} color='#d1f0ee'/>
            ),
        }}/>

    </HomeStack.Navigator>
)

const AllMushroomStackScreen = ({navigation}) => (
    <AllMushroomStack.Navigator initialRouteName="Mushrooms" screenOptions={{
        headerStyle: {
            backgroundColor: '#3CB371',
        },
        headerTintColor: '#fff',
    }}>
        <AllMushroomStack.Screen name="Mushrooms" component={AllMushroomsScreen} options={{
            title: "Mushrooms",
            headerLeft: () => (
                <Icon.Button name="menu" backgroundColor="#3CB371" size={25} onPress={() => {
                    navigation.openDrawer()
                }}/> 
            ),
            headerRight: (props) => (
                <Search {...props} navigation={navigation} color='#C4E8D4'/>
            ),
        }}/> 
        <AllMushroomStack.Screen name="Details" component={MushroomScreen} initialParams={{muhsroom: null}} options={{
            title: "Details",
            headerLeft: () => (
                <Icon.Button name="menu" backgroundColor="#3CB371" size={25} onPress={() => {
                    navigation.openDrawer()
                }}/> 
            ),
            headerRight: () => (
                <Icon.Button name="close" backgroundColor="#3CB371" size={25} onPress={() => {
                    navigation.navigate('Mushrooms');
                }}/> 
            ),
        }}/>
        <HomeStack.Screen name="CameraScreen" component={CameraScreen} />
    </AllMushroomStack.Navigator>
)

const MapStackScreen = ({navigation}) => (
    <MapStack.Navigator  screenOptions={{
        headerStyle: {
            backgroundColor: '#4169E1',
        },
        headerTintColor: '#fff',
    }}>
        <MapStack.Screen name="Locations" component={MapScreen} options={{
            title: "Locations",
            headerLeft: () => (
                <Icon.Button name="menu" backgroundColor="#4169E1" size={25} onPress={() => {
                    navigation.openDrawer()
                }}/> 
            ),
            // headerRight: (props) => (
            //     <Search {...props}/>
            // ),
        }}/>  
    </MapStack.Navigator>
)

const MainTabScreen = () => (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#fff"
      shifting={true}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Icon name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="All Mushrooms"
        component={AllMushroomStackScreen}
        options={{
            tabBarLabel: 'Mushrooms',
            tabBarColor: '#3CB371',
            tabBarIcon: ({ color }) => (
                <Icon name="mushroom" color={color} size={26} />
            ),
        }}
      />
      <Tab.Screen
        name="Map"
        component={MapStackScreen}
        options={{
            shifting: true,
            tabBarLabel: 'Map',
            tabBarColor: '#4169E1',
            tabBarIcon: ({ color }) => (
                <Icon name="map-check-outline" color={color} size={26} />
            ),
        }}
      />
    </Tab.Navigator>
)

export default MainTabScreen;


