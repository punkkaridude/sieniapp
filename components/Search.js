import React, { useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import { Icon, SearchBar } from 'react-native-elements';
import SearchContext from '../context/search.context';

export default Search = (props) => {
    const { searchquery, setSearchquery } = useContext(SearchContext);
    return (
        <View style={{ flexDirection: 'row' }}>
            <View style={styles.iconContainer}>
                <Icon
                    color="#fff"
                    name="camera"
                    style={styles.icon}
                    onPress={() => props.navigation.navigate("CameraScreen")}
                />
            </View>
            <SearchBar
                round
                containerStyle={styles.searchbarCont}
                inputContainerStyle={{backgroundColor: props.color}}
                placeholder="Search"
                onChangeText={setSearchquery}
                value={searchquery}
            />
        </View>
        
    );
};

const styles = StyleSheet.create({
    searchbarCont: {
        flex: 1, 
        backgroundColor: "transparent", 
        minWidth: 165,
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    icon: {
    },
    iconContainer: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center'
    }
});