import React, {useState} from 'react';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { useColorScheme } from 'react-native-appearance';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MainTabScreen from '../screens/MainTabScreen';
import UserScreen from '../screens/UserScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import { DrawerContent } from '../screens/DrawerContent';
import SearchContext from '../context/search.context';
import ClarifaiContext from '../context/clarifai.context';
import favoritesContext from '../context/favorites.context';

const Drawer = createDrawerNavigator();

export default SignInStack = () => {
  const [searchquery, setSearchquery] = useState("");
  const [clarifaiResult, setClarifaiResult] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const favoritesValue = {favorites, setFavorites};
  const searchValue = {searchquery, setSearchquery};
  const clarifaiValue = {clarifaiResult, setClarifaiResult};
  const scheme = useColorScheme();
  return (
  <ClarifaiContext.Provider value={clarifaiValue}>
    <favoritesContext.Provider value={favoritesValue}>
      <SearchContext.Provider value={searchValue}>
        <NavigationContainer theme={scheme === "dark" ? DarkTheme : DefaultTheme}>
          <Drawer.Navigator
            drawerContent={(props) => <DrawerContent {...props}/>}
            drawerStyle={{
              width: 300
            }}
            dra
          >
            <Drawer.Screen name="Frontpage" component={MainTabScreen} />
            <Drawer.Screen name="Profile" component={UserScreen} />
            <Drawer.Screen name="Favorites" component={FavoritesScreen} />
          </Drawer.Navigator>
        </NavigationContainer>
      </SearchContext.Provider>
    </favoritesContext.Provider>
  </ClarifaiContext.Provider>
  )
}

