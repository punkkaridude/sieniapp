import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
    webClientId: '219865068961-s4buasut0hjtr9n1n76g2mgon3jb33kh.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
    offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
});
  
export const createAccount = async (name, username, password) => {
    let userObj = { name: name, user: username };
    return await auth()
        .createUserWithEmailAndPassword(username, password)
            .then(async (credential) => {
                console.log('User account created & signed in!', username);
                await credential.user.updateProfile({
                    displayName: name
                }).then(() => console.log("User Updated"));
                return credential.user
            }).then(async (user) => {
                return await database().ref('/users').child(user.uid).set(userObj).then(() => console.log("database created")); 
            }).then(async () => {
                console.log("Finally Sign Out!");
                return await auth().signOut();
            }).finally(async () => {
                console.log("Resign In")
                await auth().signInWithEmailAndPassword(username, password).then((updatedCred) => console.log(updatedCred.user));
            });
}

export const SignInEmailAndPassword = async (username, password) => {
    return await auth()
        .signInWithEmailAndPassword(username, password)
        .then((user) => {
            console.log('User signed in!', user); 
        });
}

export const singInWithGoogle = async () => {
    await GoogleSignin.hasPlayServices(); //check that user has playservices
    const userInfo = await GoogleSignin.signIn().catch(err => {
        if (err.code === statusCodes.SIGN_IN_CANCELLED) {
            console.log(err.message);         
            return null //return userInfo as null if user cancel sign in flow
        }
    });
    //if user sign in 
    if(userInfo){ 
        console.log("Singed in with google");
        const credential = auth.GoogleAuthProvider.credential(
            userInfo.idToken, userInfo.accessToken
        );
        //signin using google idToken
        return await auth().signInWithCredential(credential).then(async credential => {
            let userObj = {
                name: credential.user.displayName,
                email: credential.user.email,
                photoURL: credential.user.photoURL
            }
            //push user data to realtime database
            const userDB = await database().ref('/users').child(credential.user.uid).once('value').then(snapshot => {
                if(snapshot){
                    return true
                } else return false
            });
            if(!userDB) database().ref('/users').child(credential.user.uid).set(userObj)
        });
    }
}

export const changePassword = async (oldpass, newpass) => {
    const user = await auth().currentUser;
    return await auth().currentUser.reauthenticateWithCredential(user.email, oldpass).then(credential => {
        return credential.user.updatePassword(newpass);
    }).then(async () => {
        console.log("Finally Sign Out!");
        return await auth().signOut();
    }).finally(async () => {
        console.log("Resign In")
        await auth().signInWithEmailAndPassword(user.email, newpass).then((updatedCred) => console.log(updatedCred.user));
    });
}

export const deleteUser = async (currentPassword) => {
    const user = await auth().currentUser;
    const userDB = await database().ref('/users/' + user.uid);
    let credentials = null;
    // console.log(user.providerData[0].providerId)
    if(user.providerData[0].providerId === 'google.com') {
        const guser = await GoogleSignin.getCurrentUser();
        credentials = await auth.GoogleAuthProvider.credential(guser.idToken);
    } else {
        credentials = await auth.EmailAuthProvider.credential(user.email, currentPassword);
    }
    // console.log(credentials)
    await userDB.remove().then(() => console.log("Database removed!"));
    await auth().currentUser.reauthenticateWithCredential(credentials)
        .then((credentials) => credentials.user.delete().then(() => console.log("User removed succesfully!")));
}

export const checkProvider = async () => {
    const user = await auth().currentUser;
    if(user.providerData){
        console.log(user.providerData[0].providerId)
        return user.providerData[0].providerId;
    }
}