import React, {useEffect, useContext, useState} from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { CommonActions } from '@react-navigation/native';
import { ListItem, Avatar, Header } from 'react-native-elements'; 
import { getAllFavorites, handleFav } from '../services/shroomservice';
import { AuthContext } from '../navigation/Navigator';

const Favorites = (props) => {
    const user = useContext(AuthContext);
    const [favorites, setFavorites] = useState([]);
    const [loading, isLoading] = useState(true);

    useEffect(() => {
        getFavs();
    },[])

    useEffect(() => {
        const reload = props.addListener('focus', () => {
          getFavs()
        });
    
        return reload;
    }, [props]);

    const getFavs = async () => {
        const favs = await getAllFavorites(user.uid).then(response => {return response});
        setFavorites(favs);
        isLoading(false);
    }

    const removeFavorite = async (id) => {
        await handleFav(id, user.uid);
        const removeFilter = favorites.filter((shroom) => shroom.id !== id);
        setFavorites(removeFilter);
    }

    const leftIcon = () => {
        return (
            <Icon.Button name="menu" backgroundColor="#3CB371" size={25} onPress={() => {
                props.openDrawer();
            }}/>
        )
    }

    const rightIcon = () => {
        return (
            <Icon.Button name="close" backgroundColor="#3CB371" size={25} onPress={() => {
                const goback = CommonActions.goBack();
                props.dispatch(goback);
            }}/> 
        )
    }

    return (
        <View style={styles.container}>
            <Header
                backgroundColor="#3CB371"
                placement="left"
                rightComponent={rightIcon()}
                leftComponent={leftIcon()}
                centerComponent={{ text: 'My Favorites', style: { color: '#fff' } }}
            />
            {
                !loading && favorites ? favorites.map((l,i) =>  (
                    <ListItem key={i} bottomDivider onPress={() => {
                        props.navigate('Frontpage', {
                            screen: 'All Mushrooms',
                            params: {
                              screen: 'Details',
                              params: {
                                mushroom: l,
                              },
                            },
                        });
                    }}>
                        <Avatar source={l.photoURL.length > 0 ? {uri: l.photoURL} : require('../assets/sieni.jpg')} />
                        <ListItem.Content>
                            <ListItem.Title>{l.name}</ListItem.Title>
                            <ListItem.Subtitle>{l.scientificName}</ListItem.Subtitle>
                        </ListItem.Content>
                        <ListItem.Chevron name="delete" color="red" size={25} onPress={() => removeFavorite(l.id)}/> 
                    </ListItem>
                )) : <View style={styles.loadingContainer}><ActivityIndicator size={80} color="#3CB371" /></View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    },
    loadingContainer: {
        flex: 1,
        justifyContent: "center",
    },
})

export default Favorites;