import React from 'react';
import Favorites from '../components/Favorites';

const FavoritesScreen = ({navigation}) => {
  return (
    <Favorites {...navigation} />
  )
};

export default FavoritesScreen;
