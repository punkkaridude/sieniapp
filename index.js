/**
 * @format
 */
import * as React from 'react';
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import { AppearanceProvider,  } from 'react-native-appearance';
import App from './App';
import { Provider as PaperProvider } from 'react-native-paper';
import {name as appName} from './app.json';

export default Main = () => {
    return (
        <AppearanceProvider>
            <PaperProvider>
                <App />
            </PaperProvider>
        </AppearanceProvider>
    )
}

AppRegistry.registerComponent(appName, () => Main);
