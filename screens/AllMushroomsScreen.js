import React, {useState, useEffect, useRef, useContext, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { View, ActivityIndicator, RefreshControl, StyleSheet, FlatList, SafeAreaView } from 'react-native';
import { FAB } from 'react-native-paper';
import SearchContext from '../context/search.context';
import ClarifaiContext from '../context/clarifai.context';
import CustomCard from '../components/Card';
import {
  getMuhsrooms, searchShrooms, searchClarifaiShroom
} from '../services/shroomservice';

const AllMushroomsScreen = ({navigation}) => {
  const [mushrooms, setMushrooms] = useState([]);
  const [results, setResults] = useState([]);
  const [loading, isLoading] = useState(true);
  const [favTrigger, setFavTrigger] = useState(false);
  const [errorMsg, setErrormsg] = useState(null);
  const [ActionbuttonVisible, setActionbuttonVisible] = useState(true);
  const [listOffset, setListOffset] = useState(0);
  const [onEndReachedCalledDuringMomentum, setonEndReachedCalledDuringMomentum ] = useState(true);
  const flatlistRef = useRef(null);

  const { searchquery } = useContext(SearchContext);
  const { clarifaiResult } = useContext(ClarifaiContext);

  useEffect(() => {
    getShrooms();
  },[])

  useFocusEffect(
    useCallback(() => {
      console.log("mushrooms focuse")
      setFavTrigger(true);
      return () => {
        console.log("mushrooms unfocuse")
        setFavTrigger(false);
      }
    }, [])
  );

  useEffect(() => {
    console.log(errorMsg)
  },[errorMsg])

  useEffect(() => {
    const cResult = searchClarifaiShroom(clarifaiResult, mushrooms);
    console.log("Clarifai result is " + clarifaiResult);
    setResults(cResult);
  },[clarifaiResult])

  useEffect(() => {
    // console.log(searchquery);
    const results = searchShrooms(searchquery, mushrooms);
    setResults(results);
  },[searchquery])
  
  const getShrooms = async () => {
    const Mushrooms = await getMuhsrooms().catch(err => setErrormsg(err));
    // console.log("get mushrooms", Mushrooms)
    setMushrooms(Mushrooms);
    if(errorMsg === null) {
      isLoading(false);
    }
  }

  const refreshAll = () => {
    // console.log("refresh");
    isLoading(true);
    setMushrooms([]);
    getShrooms();
    setResults([]);
  }

  const createCard = (item) => (
    <CustomCard data={item} navigation={navigation} trigger={favTrigger}/>
  )

  const scrollTop = () => {
    flatlistRef.current.scrollToIndex({
      animated: true, 
      index: 0, 
      viewOffset: 0, 
      viewPosition: 0,
    });
  }

  //by mmazzarolo, change state after flatlist is scrolled
  const handleOnScroll = (event) => {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = (currentOffset > 0 && currentOffset >= listOffset) ? 'down' : 'up'
    const isActionbuttonVisible = direction === 'up'
    if(isActionbuttonVisible !== ActionbuttonVisible) {
      setActionbuttonVisible(isActionbuttonVisible);
    }
  }

  const showFab = () => {
    return(
      !ActionbuttonVisible ?
      <FAB 
        style={styles.fab}
        small
        icon="chevron-up"
        onPress={() => scrollTop()} 
      /> : null
    )
  }

  return(
    loading ? <View style={styles.loadingContainer}><ActivityIndicator size={80} color="#3CB371" /></View> : 
    <SafeAreaView style={styles.container}>
      <FlatList
        ref={flatlistRef}
        numColumns={2}
        data={results.length > 0 ? results : mushrooms}
        renderItem={createCard}
        keyExtractor={(item) => item.id}
        onMomentumScrollBegin={() => setonEndReachedCalledDuringMomentum(false)}
        onScroll={handleOnScroll}
        onEndReachedThreshold={0.2}
        viewabilityConfig={{ itemVisiblePercentThreshold: 50 }}
        refreshControl={
          <RefreshControl 
            refreshing={loading}
            onRefresh={refreshAll}
          />
        }
      >
      </FlatList>
      {showFab()}
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
  },
  fab: {  
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: '#3CB371'
  }
});

export default React.memo(AllMushroomsScreen);
