import React, { useState, useEffect, useContext } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Header, Avatar, Text, Button, Input } from 'react-native-elements';
import { AuthContext } from '../navigation/Navigator';
import { Dialog, Portal, Paragraph } from 'react-native-paper';
import {
  deleteUser,
  checkProvider,
  changePassword
} from '../services/loginservice';

const UserScreen = ({navigation}) => {
  const [visible, setVisible] = useState(false);
  const [provider, setProvider] = useState("");
  const [currentPassword, setCurrentPassword] = useState("");
  const [currentPassword2, setCurrentPassword2] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [pwerrMsg, setPwerrMsg] = useState("");
  const [errMsg, setErrMsg] = useState("");
  const user = useContext(AuthContext);

  useEffect(() => { 
    // console.log(user)
    checkProv(); 
  },[])

  const checkProv = async () => {
    const prov = await checkProvider();
    setProvider(prov);
  }

  const leftIcon = () => {
    return (
        <Icon.Button name="menu" backgroundColor="#3CB371" size={25} onPress={() => {
            navigation.openDrawer();
        }}/>
    )
}

  const rightIcon = () => {
      return (
          <Icon.Button name="close" backgroundColor="#3CB371" size={25} onPress={() => {
              const goback = CommonActions.goBack();
              navigation.dispatch(goback);
          }}/> 
      )
  }

  const changePassw = async () => {
    try {
      if(currentPassword === currentPassword2 && currentPassword !== "" && currentPassword2 !== ""){
        await changePassword(currentPassword, newPassword);
      } else {
        setPwerrMsg("Passwords don't match");
      }
    } catch (err) {
      setPwerrMsg(err.message);
    }
  }

  const handleDelete = async () => {
    try {
      console.log("delete account");
      if(currentPassword !== "") await deleteUser(currentPassword);
      else setErrMsg("Need current password");
    } catch(err) {
      console.log(err.message);
      setErrMsg(err.message);
    }
  }

  const showDialog = () => {
    console.log(provider)
    setVisible(true);
  }

  const hideDialog = () => {
    setVisible(false);
    setCurrentPassword("");
    setErrMsg("");
  }

  return (
    <ScrollView style={styles.container}>
      <Header
        backgroundColor="#3CB371"
        placement="left"
        rightComponent={rightIcon()}
        leftComponent={leftIcon()}
        centerComponent={{ text: 'Profile', style: { color: '#fff' } }}
      />
      <View style={styles.content}>
        {user.photoURL ? <Avatar 
          onPress={() => console.log("avatar press")}
          rounded
          size="xlarge"
          source={{ uri: user.photoURL}}
        /> : <Avatar 
        onPress={() => console.log("avatar press")}
        rounded
        size="xlarge"
        overlayContainerStyle={{backgroundColor: "grey"}}
        icon={{name: 'user', type: 'font-awesome'}}
        /> }
        <Text h3>{user.displayName}</Text>
        <Paragraph>{user.email}</Paragraph> 
        { provider !== "google.com" ?<View style={styles.form}>
          <Input 
            secureTextEntry={true} 
            label="Current password:" 
            onChangeText={value => {
              setCurrentPassword(value);
            }}
          />
          <Input 
            secureTextEntry={true} 
            label="Repeat password:" 
            onChangeText={value => {
              setCurrentPassword2(value);
            }}
          />
          <Input 
            secureTextEntry={true} 
            label="New password:" 
            onChangeText={value => {
              setNewPassword(value);
            }}
            errorMessage={pwerrMsg}
          />
          <Button
            buttonStyle={styles.changePw}
            titleStyle={{fontSize: 20}} 
            iconRight
            icon={
              <Icon name="lock" color="#fff" size={25}/>
            }
            title="Change Password"
            onPress={() => changePassw()}
          />
        </View> : null}
        
        <Button
          buttonStyle={styles.delete}
          titleStyle={{fontSize: 20}} 
          iconRight
          icon={
            <Icon name="delete" color="#fff" size={25}/>
          }
          title="Delete account"
          onPress={() => showDialog()}
        />
      </View>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Create a New Account</Dialog.Title>
          <Text>Are you sure you want to remove user and all saved user data?</Text>
          <Dialog.Content>
            <Text>{errMsg}</Text>
            {provider !== "google.com" ? <Input 
              secureTextEntry={true} 
              label="Current password:" 
              onChangeText={value => {
                setCurrentPassword(value);
              }}
            /> : null}
          </Dialog.Content>
          <Dialog.Actions>
            <Button title="Cancel" onPress={hideDialog} />
            <Button title="Delete User Permanently" onPress={handleDelete} />
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </ScrollView>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    marginTop: 50
    
  },
  delete: {
    backgroundColor: "red",
    marginTop: 20
  },
  changePw: {
    backgroundColor: "green",
    marginTop: 20
  },
  form: {
    marginTop: 50,
    width: "85%",
  },
});

export default UserScreen;
