import React, { useState } from 'react';
import { StyleSheet, View} from 'react-native';
import { Input, Button, Text } from 'react-native-elements';
import { Dialog, Portal } from 'react-native-paper';
import {
  createAccount,
  SignInEmailAndPassword,
  singInWithGoogle
} from '../services/loginservice';

const LoginScreen = () => {
  const [visible, setVisible] = useState(false);
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [loginErrMsg, setLoginErrMsg] = useState("");
  const [errMsg, setErrMsg] = useState("");

  const signUp = async () => {
    try {
      if(name && username && password && password2){
        if(password === password2) {
          await createAccount(name, username, password);
        }
        else {
          setErrMsg("Passwords don't match")
        }
      }
    } catch (err) {
      setErrMsg(err.message);
    } 
  }

  const signIn = async () => {
    try {
      if(username && password){
        await SignInEmailAndPassword(username, password);
      }
    } catch (err) {
      setLoginErrMsg(err.message);
    }
  }

  const googleSignIn = async () => {
    try {
      await singInWithGoogle();
    } catch (err) {
      setLoginErrMsg(err.message)
    }
  }

  const hideSignup = () => {
    setVisible(false);
    setUsername("");
    setPassword("");
    setPassword2("");
    setErrMsg("");
    setLoginErrMsg("");
    setName("");
  }

  return (
    <View style={styles.container}>
      <Text h2 style={styles.headerText}>SieniApp</Text>
      <Text h4 style={styles.headerText}>Login</Text>
      <View style={styles.form}>
        <Text>{loginErrMsg}</Text>
        <Input labelStyle={styles.label} inputContainerStyle={styles.inputCont} label="Email:" onChangeText={value => {
          setUsername(value);
        }}/>
        <Input secureTextEntry={true} labelStyle={styles.label}  inputContainerStyle={styles.inputCont} label="Password: " onChangeText={value => {
          setPassword(value);
        }}/>
        <Button buttonStyle={{marginBottom: 10}} title="Sign In" onPress={() => signIn()} />
        <Button title="Sign Up" onPress={() => setVisible(true)} />
        <Text h4 style={styles.textCenter}>Or</Text>
        <Button title="Sign in with Google" onPress={() => googleSignIn()} />
      </View>
      <Portal>
        <Dialog visible={visible} onDismiss={hideSignup}>
          <Dialog.Title>Create a New Account</Dialog.Title>
          <Text>{errMsg}</Text>
          <Dialog.Content>
            <Input
              textContentType="emailAddress"
              value={username} 
              placeholder="Email" 
              onChangeText={value => {
                setUsername(value);
              }}
            />
            <Input
              textContentType="name" 
              value={name} 
              placeholder="Name " 
              onChangeText={value => {
                setName(value);
              }}
            />
            <Input 
              textContentType="password"
              secureTextEntry={true} 
              value={password} 
              placeholder="Password " 
              onChangeText={value => {
                setPassword(value);
              }}
            />
            <Input 
              textContentType="password"
              secureTextEntry={true} 
              value={password2} 
              placeholder="Repeat Password " 
              onChangeText={value => {
                setPassword2(value);
              }}
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button title="Cancel" onPress={hideSignup} />
            <Button title="Sign Up" onPress={signUp} />
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3CB371',
  },
  headerText: {
    color: "#fff",
    textAlign: 'center',
  },
  textCenter: {
    color: "#fff",
    textAlign: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  form: {
    width: "85%",
  },
  label: {
    color: "#fff",
  },
  inputCont: {
    borderColor: "#fff",
  }
});

export default LoginScreen;
