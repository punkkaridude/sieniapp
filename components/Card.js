import React, {useState, useEffect, useContext} from 'react';
import { StyleSheet } from 'react-native';
import { Card, Paragraph, Title } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
    checkFav,
    handleFav,
} from '../services/shroomservice';
import { AuthContext } from '../navigation/Navigator';

const CustomCard = (props) => {
    const [favorite, setFavorite] = useState(false);
    const user = useContext(AuthContext)
    const [imgurl, setImgurl] = useState(null);
    const shroom = props.data.item;
    const favTrigger = props.trigger;
    // console.log(shroom);

    useEffect(() => {
        checkFavorite();
    },[favTrigger])

    useEffect(() => {
        if(shroom.lajidata !== null) setImgurl(shroom.lajidata.media.bigPhoto);
        checkFavorite();
    },[shroom])

    const checkFavorite = async () => {
        const isfavorited = await checkFav(shroom.id, user.uid).then((response) => {return response});
        setFavorite(isfavorited);
    }

    const handleFavorite = async () => {
        console.log("favorite");
        setFavorite(!favorite);
        await handleFav(shroom.id, user.uid).then((response) => {return response});
    }

    const handlePress = () => {
        props.navigation.push('Details', {
            mushroom: shroom
        });
    }

    return(
        <Card key={shroom.id} style={styles.container} onPress={handlePress}>
            <Card.Cover source={
                 shroom.photoURL ? { uri: shroom.photoURL } 
                : imgurl ? { uri: imgurl } 
                : require("../assets/sieni.jpg") }
            />
            <Icon name="heart" size={27} color={favorite ? "red" : "grey"}  style={styles.Heart} onPress={() => handleFavorite()}/>
            <Card.Content style={styles.Content}> 
                <Title style={[styles.textCenter, styles.Title]}>{shroom.name}</Title>
                <Paragraph style={[styles.textCenter, styles.Paragraph]}>{shroom.scientificName}</Paragraph>
            </Card.Content>
        </Card>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 2
    },
    textCenter: {
        textAlign: "center",
        marginBottom: 0,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
    },
    Paragraph: {
        fontSize: 12,
    },
    Title: {
        fontSize: 15,
    },
    Content: {
        flex: 1,
        marginBottom: 0, 
        paddingBottom: 10,
    },
    Heart: {
        position: "absolute",
        right: 0,
        alignSelf: "center",
        paddingRight: 10,
        paddingTop: 10,
    }

})

export default CustomCard;