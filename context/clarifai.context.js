import React, { createContext } from 'react';

const ClarifaiContext = createContext({
    clarifaiResult: null,
    setClarifaiResult: () => {}
});

export default ClarifaiContext;