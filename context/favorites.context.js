import React, { createContext } from 'react';

const favoritesContext = createContext({
    favorites: [],
    setFavorites: () => {}
});

export default favoritesContext;