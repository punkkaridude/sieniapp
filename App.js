import React from 'react';
import Navigator from './navigation/Navigator';

class App extends React.Component {
  render() {
    return (
      <Navigator />
    );
  }
}

export default App;
