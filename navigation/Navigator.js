import React, { useState, useEffect, createContext } from 'react';
import auth from '@react-native-firebase/auth';
import SignInStack from './SignInStack';
import SignOutStack from './SignOutStack';

export const AuthContext = createContext(null);
export default function Navigator() {
    const [ user, setUser ] = useState();

    useEffect(() => {
        const unsub = auth().onAuthStateChanged(user => {
            if(user && user.displayName !== null) setUser(user)
            else setUser();
        });
        return unsub;
    }, [])

    return user ? (
        <AuthContext.Provider value={user}>
            <SignInStack />
        </AuthContext.Provider> ) : ( <SignOutStack /> )
}