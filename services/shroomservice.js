import database from '@react-native-firebase/database';
import axios from 'axios';
import {API_TOKEN_LAJI} from '@env';
import { Value } from 'react-native-reanimated';

const db = database().ref('/shrooms');

//gets all mushroom from firebase
export const getMuhsrooms = async () => {
  const shroomArray = await db.orderByChild("name").once("value").then(async snapshot => {
    let data = [];
    snapshot.forEach(item => {
      data.push({...item.val()});  
    });
    return data;
  });

  const promise = shroomArray.map(async item => {
    if(item.photoURL === ""){
      const lajidata = await getLajidata(item.scientificName);
      return {...item, lajidata};
    } else return {...item, lajidata: null}
  })

  const results = Promise.all(promise).then(result => result);
  return results;
}

//search filter (local)
export const searchShrooms = (query, data) => {
  if(query){
    const searchResults = data.filter(item => {
      const itemData = `${item.name.toUpperCase()} ${item.scientificName.toUpperCase()} `;
      const searchText = query.toUpperCase();
      return itemData.indexOf(searchText) > -1;
    })
    return searchResults;
  } else {
    return [];
  }
}

//clarifai result filter (local)
export const searchClarifaiShroom = (query, data) => {
  if(query){
    const searchResults = data.filter(item => {
      const itemData = `${item.id}`;
      const searchText = query;
      return itemData.indexOf(searchText) > -1;
    })
    console.log(searchResults);
    return searchResults;
  } else {
    return [];
  }
}

export const getAllFavorites = async (uid) => {
  let favdb = database().ref('/users/' + uid + '/fav');
  const favIDs = await favdb.once("value").then(snapshot => {
    let data = [];
    snapshot.forEach(async (item) => {
      data.push(item.val().id);
    });
    return data;
  });
  
  const favPromises = favIDs.map(id => {
    return db.orderByChild("id").equalTo(id).once('value').then(async s => {
      // console.log(s.val())
      const mushroom = Object.values(s.val())[0];
      const lajidata = await getLajidata(mushroom.scientificName);
      // console.log({...mushroom, lajidata})
      return {...mushroom, lajidata}
    });
  })

  const result = Promise.all(favPromises).then(shrooms => {
    let results = [];
    shrooms.forEach(value => {
      results.push(value);
    });
    return results;
  })
  return result;
}

export const getFavoriteCount = async (uid) => {
  let favdb = database().ref('/users/' + uid + '/fav');
  return await favdb.once("value").then(snapshot => {
    let count = 0;
    snapshot.forEach(() => count++);
    return count;
  });
}

export const handleFav = (id, uid) => {
  let db = database().ref('/users/' + uid + '/fav');
  return db.orderByChild("id").equalTo(id).once("value").then(snapshot => {
    if(snapshot.val()){
      snapshot.forEach((child) => {
        child.ref.remove()
          .then(() => {
            console.log('data removed');
          })
          .catch((error) => console.log('error ', error));
      })
      return false;
    } else {
      console.log("No in favorites");
      db.push({ id: id })
        .then((data) => { 
          console.log('data ', data); 
        })
        .catch((error) => { console.log('error ', error); });
      return true;
    }
  })
}

export const checkFav = (id, uid) => {
  let db = database().ref('/users/' + uid + '/fav');
  return db.orderByChild('id').equalTo(id).once("value").then(snapshot => {
    if(snapshot.val()){
     return true;
    } else return false;
  })
}

export const getLajidata = async (name) => {
  const URL = "https://api.laji.fi/v0/taxa/search?query=";
  const URLEND = "&limit=10&informalTaxonGroup=MVL.233&includeHidden=false&matchType=exact&onlySpecies=false&onlyFinnish=false&onlyInvasive=false&observationMode=false&access_token=";
  const URL2 = "https://api.laji.fi/v0/taxa/";
  const URLEND2 = "/media?access_token=";
  const URLEND3 = "/descriptions?lang=fi&langFallback=true&access_token=";
  
  let newName = name.replace(" ", "%20");

  const id = await axios.get( URL + newName + URLEND + API_TOKEN_LAJI ).then(response => {
    // console.log("response", response.data)
    if(response.data.length !== 0){
      return response.data[0].id
    } return null
  });
  
  if (id) {
    const media = axios.get(URL2 + id + URLEND2 + API_TOKEN_LAJI );
    const description = axios.get( URL2 + id + URLEND3 + API_TOKEN_LAJI );
  
    const lajiData =await axios.all([media, description]).then(axios.spread((...response) => {
      let lajiid = id;
      let media = {};
      let description = "";
      let habitat = "";
      if(response[0].data.length > 0){
        media = {
          bigPhoto: response[0].data[0].fullURL,
          smallPhoto: response[0].data[0].thumbnailURL 
        }
      }
      if(response[1].data.length > 0){
        //console.log(response.data)
        if(response[1].data[0].groups[0]){
          let desc = response[1].data[0].groups[0].variables[0].content.replace('<p>', '');
          description = desc.replace('</p>','');
        }
        if(response[1].data[0].groups[3]){
          let habt = response[1].data[0].groups[3].variables[0].content.replace('<p>', '');
          habitat = habt.replace('</p>', ''); 
        } 
      }
      return { lajiid, media, description, habitat }
    })).catch(error => {console.log("Error fetching lajidata: ", error)});
    // console.log(lajiData)
    return lajiData;
  }
  return null;
}
