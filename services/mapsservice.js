import database from '@react-native-firebase/database';

const db = database().ref('/users');

//get all locations from users database 
export const getLocations = async (uid) => {
    return await db.child(uid + '/places').once("value").then(snapshot => {
        let data = [];
        snapshot.forEach(item => {
            // console.log(item.val())
            data.push(item.val());
        })
        return data;
    });
}

export const getLocationCount = async (uid) => {
    return await db.child(uid + '/places').once("value").then(snapshot => {
        let count = 0;
        snapshot.forEach(() => count++)
        return count;
    });
}

//add place to users database
export const addLocation = async (places, uid) => {
    return await db.child(uid + '/places').push(places);
}
 
//remove place from users database
export const deleteLocation = async (id, uid) => {
    return await db.child(uid + '/places').orderByChild('id').equalTo(id).once('value').then(snapshot => {
        if(snapshot.val()){
            snapshot.forEach(item => {
                item.ref.remove().then(() => console.log("Location deleted"))
            })
        }
    })
}


