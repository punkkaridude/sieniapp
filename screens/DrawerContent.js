import React, {useContext, useState} from 'react';
import { View, StyleSheet } from 'react-native';
import auth from '@react-native-firebase/auth';
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem,
    useIsDrawerOpen
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { getLocationCount } from '../services/mapsservice';
import { getFavoriteCount } from '../services/shroomservice';
import { AuthContext } from '../navigation/Navigator';

export function DrawerContent(props) {
    const isDrawerOpen = useIsDrawerOpen();
    const [lcount, setLcount] = useState(0);
    const [mcount, setMcount] = useState(0);
    const user = useContext(AuthContext);
    
    const getCounts = async() => {
        setLcount(await getLocationCount(user.uid));
        setMcount(await getFavoriteCount(user.uid));
    }
    
    if(isDrawerOpen) getCounts();

    return (
        <View style={{flex: 1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={styles.avatarTitlearea}>
                            {user ? <Avatar.Image 
                                source={{uri:user.photoURL}}
                                size={45}
                            /> : <Avatar.Icon 
                                icon="user"
                                size={45}
                            />}
                            <View style={{marginLeft: 15}}>
                                <Title style={styles.title}>{user.displayName}</Title>
                                <Caption style={styles.caption}>
                                    {user.email}
                                </Caption>
                            </View>
                        </View>
                        <View style={{flexDirection: 'column', marginTop: 15}}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>{mcount}</Paragraph>
                                <Caption style={styles.caption}>Favorite Mushrooms</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>{lcount}</Paragraph>
                                <Caption style={styles.caption}>Own locations</Caption>
                            </View>
                        </View>
                    </View>
                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                    name="account-outline"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Profile"
                            onPress={() => {props.navigation.navigate('Profile')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                    name="mushroom-outline"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Favorites"
                            onPress={() => {props.navigation.navigate('Favorites')}}
                        />
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section styles={styles.bottomDrawerSection}>
                <Drawer.Item 
                    icon={({color, size}) => (
                        <Icon 
                            name="exit-to-app" 
                            color={color}
                            size={size}
                        />
                    )}
                    label="Sign Out" 
                    onPress={async () => {
                        try {
                            await auth().signOut().then(() => console.log("User signed out!"));
                        } catch (error) {
                            console.error(error);
                        }
                    }}
                />
            </Drawer.Section>
        </View>
    )
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    avatarTitlearea: {
        flexDirection: 'row', 
        marginTop: 15, 
        alignItems: 'center'
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
})