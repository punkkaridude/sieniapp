import React, {useState, useContext, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { Card, Title, Paragraph } from 'react-native-paper';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import {
  checkFav,
  handleFav
} from '../services/shroomservice';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthContext } from '../navigation/Navigator';

const MushroomScreen = ({route}) => {
  const [shroom, setShroom] = useState(null);
  // const [lajidata, setLajidata] = useState(null);
  const [favorited, setFavorite] = useState(false); 
  const [loading, isLoading] = useState(true);
  const user = useContext(AuthContext); 
  const { mushroom } = route.params;

  useFocusEffect(
    useCallback(() => {
      console.log("mushroom focuse")
      // getData();
      checkFavorite();
      setShroom(mushroom);

      return () => {
        console.log("mushroom unfocuse")
        setShroom(null);
        // setLajidata(null);
        isLoading(true);
      }
    }, [])
  );

  // const getData = async () => {
  //   const data = await getLajidata(mushroom.scientificName ? mushroom.scientificName : mushroom.name);    
  //   setLajidata(data);
    
  // }

  const checkFavorite = async () => {
    const isfavorited = await checkFav(mushroom.id, user.uid).then((response) => {return response});
    setFavorite(isfavorited);
    isLoading(false);
  }

  const handleFavorite = async () => {
    console.log("favorite")
    const isfavorited = await handleFav(mushroom.id, user.uid).then((response) => {return response});
    setFavorite(isfavorited);
  }

  return (
      loading ? <View style={styles.loadingContainer}><ActivityIndicator size={80} color="#3CB371" /></View> :
      <View style={styles.container}>
        <Card>
          <Card.Title 
            title={shroom.name} 
            subtitle={shroom.scientificName}
            right={(props) => 
              <Icon {...props} 
                name="heart"
                color={favorited ? "red" : "grey"} 
                style={styles.heart}
                onPress={() => handleFavorite()}
              />
            }
          />
          {shroom.lajidata !== null ? 
            <Card.Cover source={{uri: shroom.lajidata.media.bigPhoto ? shroom.lajidata.media.bigPhoto : shroom.photoURL}}/> : 
            <Card.Cover source={require('../assets/sieni.jpg')}/>
          }
          <Card.Content>
            <Title>Kuvaus:</Title>
            <Paragraph>{shroom.description !== "" ? shroom.description : shroom.lajidata !== null ? shroom.lajidata.description : ""}</Paragraph>
          </Card.Content>
          <Card.Content>
            <Title>Kasvuympäristö:</Title>
            <Paragraph>{shroom.habitat !== "" ? shroom.habitat : shroom.lajidata !== null ? shroom.lajidata.habitat : ""}</Paragraph>
          </Card.Content>
        </Card>
      </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
  },
});

export default MushroomScreen;
