import React, {useContext, useState, useCallback} from 'react';
import {StyleSheet, Dimensions, ImageBackground, Image, View } from 'react-native';
import { AuthContext } from '../navigation/Navigator';
import { useFocusEffect } from '@react-navigation/native';
import { Card, Paragraph, Title, Text } from 'react-native-paper';
import Carousel from 'react-native-snap-carousel';
import { getAllFavorites } from '../services/shroomservice.js';

const WelcomeCard = () => {
  const user = useContext(AuthContext);
  return (
      <View style={styles.card}>
        <ImageBackground source={require('../assets/welcome.jpg')} style={styles.image}>
          <Text style={styles.title}>Welcome, {user.displayName}</Text>
        </ImageBackground>
      </View>
  )
}

const FrontpageScreen = ({navigation, route}) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const [favs, setFavs] = useState([]);
  const user = useContext(AuthContext);
  const screenWidth = Math.round(Dimensions.get('window').width);

  useFocusEffect(
    useCallback(() => {
      console.log("Home focuse")
      getAllfavs();
      return () => {
        console.log("Home unfocuse")
      }
    }, [])
  );

  const getAllfavs = async () => {
    const favorite = await getAllFavorites(user.uid);
    // console.log(favorite)
    setFavs(favorite);
  }

  const handleFavPress = (item) => {
    navigation.navigate('Frontpage', {
      screen: 'All Mushrooms',
      params: {
        screen: 'Details',
        params: {
          mushroom: item,
        },
      },
  });
  }

  const renderItem = ({item, index}) => {
    // console.log(item)
    return (
      <Card key={item.id} style={styles.container} onPress={() => handleFavPress(item)}>
        {item.lajidata ? 
        <Card.Cover style={{resizeMode: 'cover'}} source={ item.lajidata.media.bigPhoto ? { uri: item.lajidata.media.bigPhoto } : require("../assets/sieni.jpg")} />
        : <Card.Cover style={{resizeMode: 'cover'}} source={ item.photoURL ? { uri: item.photoURL } : require("../assets/sieni.jpg")} />}
        <Card.Content style={styles.Content}> 
            <Title style={[styles.textCenter, styles.Title]}>{item.name}</Title>
            <Paragraph style={[styles.textCenter, styles.Paragraph]}>{item.scientificName}</Paragraph>
        </Card.Content>
      </Card>
    )
  }

  return (
    <View style={{flex: 1}}>
      <WelcomeCard />
      <Text style={{fontSize: 20, fontWeight: "700", textAlign: 'center', paddingTop: 20, paddingBottom: 20}}>Your favorites</Text>
      {favs ? 
      <Carousel 
        enableSnap={true}
        layout={"default"}
        data={favs}
        renderItem={renderItem}
        sliderWidth={screenWidth}
        itemWidth={screenWidth-100}
        onSnapToItem={ index => setActiveIndex(index)}
      /> 
      : null}
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2
  },
  textCenter: {
      textAlign: "center",
      marginBottom: 0,
      marginTop: 0,
      marginLeft: 0,
      marginRight: 0,
  },
  Paragraph: {
      fontSize: 12,
  },
  Title: {
      fontSize: 15,
  },
  Content: {
      flex: 1,
      marginBottom: 0, 
      paddingBottom: 10,
      alignItems: 'center',
      justifyContent: 'center'
  },
  card: {
    height: 300
  },
  title: {
      fontSize: 28,
      fontWeight: 'bold',
      color: '#fff',
      position: 'relative'

  },
  image: {
      flex: 1,
      resizeMode: 'cover',
      padding: 30
  }
});

export default FrontpageScreen;
